<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="Content-Style-Type" content="text/css">
<meta http-equiv="Content-Script-Type" content="text/javascript">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<link rel="stylesheet" type="text/css" href="../../../system/css/global.css">
<link rel="stylesheet" type="text/css" href="../css/contents.css">
<link rel="stylesheet" type="text/css" href="../../../system/css/facebox.css">
<link rel="stylesheet" type="text/css" href="../css/print.css" id="print_css">
<script type="text/javascript" src="../../../system/js/util.js?cntl=Contents" charset="UTF-8"></script>
<script type="text/javascript" src="../../../system/js/dict_const.js" charset="UTF-8"></script>
<script type="text/javascript" src="../../../system/js/dict_message.js" charset="UTF-8"></script>
<script type="text/javascript" src="../../../system/js/use.js" charset="UTF-8"></script>
<script type="text/javascript" src="../../../system/js/contents.js" charset="UTF-8"></script>
<title>Toyota Service Information</title>
</head>
<body>
<div id="wrapper">
<div id="header"></div>
<div id="body">
<div id="contents">
<div id="contentsBody" class="fontEn">
<div class="globalInfo">
<span class="globalPubNo">RM27T0E</span>
<span class="globalServcat">_V1</span>
<span class="globalServcatName">General</span>
<span class="globalSection">_053652</span>
<span class="globalSectionName">INTRODUCTION</span>
<span class="globalTitle">_0260208</span>
<span class="globalTitleName">HOW TO TROUBLESHOOT ECU CONTROLLED SYSTEMS</span>
<span class="globalCategory">F</span>
</div>
<h1>INTRODUCTION&nbsp;&nbsp;HOW TO TROUBLESHOOT ECU CONTROLLED SYSTEMS&nbsp;&nbsp;GENERAL INFORMATION&nbsp;&nbsp;</h1>
<br>
<div id="RM100000000VO5N_z0" class="category no00">
<div class="content5">
<div class="list1">
<div class="list1Item">
<div class="list1Head"></div>
<div class="list1Body"><p>A large number of ECU controlled systems are used in the LAND CRUISER, LAND CRUISER PRADO. In general, ECU controlled systems are considered to be very intricate, requiring a high level of technical knowledge to troubleshoot. However, most problem checking procedures only involve inspecting the ECU controlled system's circuits one by one. An adequate understanding of the system and a basic knowledge of electricity is enough to perform effective troubleshooting, accurate diagnoses and necessary repairs.
</p>
</div>
</div>
</div>
<br>
<div class="step1">
<p class="step1"><span class="titleText">TROUBLESHOOTING PROCEDURES
</span></p>
<br>
<div class="list1">
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>The troubleshooting procedures consist of diagnosis procedures for when a DTC is stored and diagnosis procedures for when no DTC is stored. The basic idea is explained in the following table.
</p>
<table summary="">
<colgroup>
<col style="width:33%">
<col style="width:33%">
<col style="width:33%">
</colgroup>
<thead>
<tr>
<th class="alcenter">Procedure Type
</th>
<th class="alcenter">Details
</th>
<th class="alcenter">Troubleshooting Method
</th>
</tr>
</thead>
<tbody>
<tr>
<td class="alleft">DTC Based Diagnosis
</td>
<td class="alleft">The diagnosis procedure is based on the DTC that is stored.
</td>
<td class="alleft">The malfunctioning part is identified based on the DTC detection conditions using a process of elimination.
<br>
The possible trouble areas are eliminated one-by-one by use of the intelligent tester and inspection of related parts.
</td>
</tr>
<tr>
<td class="alleft">Symptom Based Diagnosis
<br>
(No DTCs stored)
</td>
<td class="alleft">The diagnosis procedure is based on problem symptoms.
</td>
<td class="alleft">The malfunctioning part is identified based on the problem symptoms using a process of elimination.
<br>
The possible trouble areas are eliminated one-by-one by use of the intelligent tester and inspection of related parts.
</td>
</tr>
</tbody>
</table>
<br>
</div>
</div>
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>Vehicle systems are complex and use many ECUs that are difficult to inspect independently. Therefore, a process of elimination is used, where components that can be inspected individually are inspected, and if no problems are found in these components, the related ECU is identified as the problem and replaced.
</p>
</div>
</div>
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>It is extremely important to ask the customer about the environment and the conditions present when the problem occurred (Customer Problem Analysis). This makes it possible to simulate the conditions and confirm the symptom. If the symptom cannot be confirmed or the DTC does not recur, the malfunctioning part may not be identified using the troubleshooting procedure, and the ECU for the related system may be replaced even though it is not defective. If this happens, the original problem will not be solved.
</p>
</div>
</div>
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>In order to prevent endless expansion of troubleshooting procedures, the troubleshooting procedures are written with the assumption that multiple malfunctions do not occur simultaneously for a single problem symptom.
</p>
</div>
</div>
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>To identify the malfunctioning part, troubleshooting procedures narrow down the target by separating components, ECUs and wire harnesses during the inspection. If the wire harness is identified as the cause of the problem, it is necessary to inspect not only the connections to components and ECUs but also all of the wire harness connectors between the component and the ECU.
</p>
</div>
</div>
</div>
<br>
</div>
<br>
<div class="step1">
<p class="step1"><span class="titleText">DESCRIPTION
</span></p>
<br>
<p>System data and the Diagnostic Trouble Codes (DTCs) can be read from the Data Link Connector 3 (DLC3) of the vehicle. When the system seems to be malfunctioning, use the intelligent tester to check for a malfunction and perform repairs.
</p>
</div>
<br>
<div class="step1">
<p class="step1"><span class="titleText">DATA LINK CONNECTOR 3 (DLC3)
</span></p>
<br>
<div class="step2">
<div class="step2Item step2HasFigure">
<div class="step2Head">a.</div>
<div class="step2Body">
<div class="figureAPatternGroup">
<div class="figure">
<div class="aPattern">
<div class="graphic">
<img src="../img/png/H100769E16.png" alt="H100769E16" title="H100769E16">
<div class="indicateinfo">
<span class="caption">
<span class="points">1.054,0.286 1.304,0.433</span>
<span class="captionSize">0.25,0.147</span>
<span class="fontsize">10</span>
<span class="captionText">CG</span>
</span>
<span class="caption">
<span class="points">1.454,0.297 1.76,0.452</span>
<span class="captionSize">0.306,0.155</span>
<span class="fontsize">10</span>
<span class="captionText">SG</span>
</span>
<span class="caption">
<span class="points">2.355,1.816 2.687,1.985</span>
<span class="captionSize">0.332,0.169</span>
<span class="fontsize">10</span>
<span class="captionText">BAT</span>
</span>
<span class="caption">
<span class="points">2.499,0.297 2.727,0.444</span>
<span class="captionSize">0.228,0.147</span>
<span class="fontsize">10</span>
<span class="captionText">SIL</span>
</span>
<span class="caption">
<span class="points">1.849,0.302 2.284,0.457</span>
<span class="captionSize">0.435,0.155</span>
<span class="fontsize">10</span>
<span class="captionText">CANH</span>
</span>
<span class="caption">
<span class="points">1.491,1.827 1.927,1.982</span>
<span class="captionSize">0.435,0.155</span>
<span class="fontsize">10</span>
<span class="captionText">CANL</span>
</span>
<span class="line">
<span class="points">2.332,1.436 2.52,1.818</span>
<span class="whiteedge">false</span>
</span>
<span class="line">
<span class="points">1.966,1.444 1.699,1.827</span>
<span class="whiteedge">false</span>
</span>
<span class="line">
<span class="points">1.219,0.465 1.606,0.905</span>
<span class="whiteedge">false</span>
</span>
<span class="line">
<span class="points">1.603,0.487 1.781,0.881</span>
<span class="whiteedge">false</span>
</span>
<span class="line">
<span class="points">1.964,0.891 2.11,0.469</span>
<span class="whiteedge">false</span>
</span>
<span class="line">
<span class="points">2.151,0.899 2.616,0.487</span>
<span class="whiteedge">false</span>
</span>
</div>
</div>
</div>
</div>
<br>
</div>
<p>The vehicle ECUs use ISO 15765-4 communication protocol. The terminal arrangement of the DLC3 complies with ISO 15031-3 and matches the ISO 15765-4 format.
</p>
<table summary="">
<colgroup>
<col style="width:25%">
<col style="width:25%">
<col style="width:25%">
<col style="width:25%">
</colgroup>
<thead>
<tr>
<th class="alcenter">Terminal No. (Symbol)
</th>
<th class="alcenter">Terminal Description
</th>
<th class="alcenter">Condition
</th>
<th class="alcenter">Specified Condition
</th>
</tr>
</thead>
<tbody>
<tr>
<td class="alcenter">7 (SIL) - 5 (SG)
</td>
<td class="alcenter">Bus "+" line
</td>
<td class="alcenter">During transmission
</td>
<td class="alcenter">Pulse generation
</td>
</tr>
<tr>
<td class="alcenter">4 (CG) - Body ground
</td>
<td class="alcenter">Chassis ground
</td>
<td class="alcenter">Always
</td>
<td class="alcenter">Below 1 Ω
</td>
</tr>
<tr>
<td class="alcenter">5 (SG) - Body ground
</td>
<td class="alcenter">Signal ground
</td>
<td class="alcenter">Always
</td>
<td class="alcenter">Below 1 Ω
</td>
</tr>
<tr>
<td class="alcenter">16 (BAT) - Body ground
</td>
<td class="alcenter">Battery positive
</td>
<td class="alcenter">Always
</td>
<td class="alcenter">11 to 14 V
</td>
</tr>
<tr>
<td class="alcenter">6 (CANH) - 14 (CANL)
</td>
<td class="alcenter">CAN bus line
</td>
<td class="alcenter">Ignition switch off*
</td>
<td class="alcenter">54 to 69 Ω
</td>
</tr>
<tr>
<td class="alcenter">6 (CANH) - 4 (CG)
</td>
<td class="alcenter">HIGH-level CAN bus line
</td>
<td class="alcenter">Ignition switch off*
</td>
<td class="alcenter">200 Ω or higher
</td>
</tr>
<tr>
<td class="alcenter">14 (CANL) - 4 (CG)
</td>
<td class="alcenter">LOW-level CAN bus line
</td>
<td class="alcenter">Ignition switch off*
</td>
<td class="alcenter">200 Ω or higher
</td>
</tr>
<tr>
<td class="alcenter">6 (CANH) - 16 (BAT)
</td>
<td class="alcenter">HIGH-level CAN bus line
</td>
<td class="alcenter">Ignition switch off*
</td>
<td class="alcenter">6 kΩ or higher
</td>
</tr>
<tr>
<td class="alcenter">14 (CANL) - 16 (BAT)
</td>
<td class="alcenter">LOW-level CAN bus line
</td>
<td class="alcenter">Ignition switch off*
</td>
<td class="alcenter">6 kΩ or higher
</td>
</tr>
</tbody>
</table>
<br>
<dl class="atten3">
<dt class="atten3">NOTICE:</dt>
<dd class="atten3">
<p>*: Before measuring the resistance, leave the vehicle as is for at least 1 minute and do not operate the ignition switch, any other switches, or the doors.
</p>
</dd>
</dl>
<br>
<p>If the result is not as specified, the DLC3 may have a malfunction. Repair or replace the harness and connector.
</p>
</div>
</div>
</div>
<br>
<div class="step2">
<div class="step2Item">
<div class="step2Head">b.</div>
<div class="step2Body">
<p>Connect the cable of the intelligent tester to the DLC3, turn the ignition switch on and attempt to use the tester. If the display indicates that a communication error has occurred, there is a problem either with the vehicle or with the tester.
</p>
<dl class="atten4">
<dt class="atten4">HINT:</dt>
<dd class="atten4"><div class="list1">
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>If communication is normal when the tester is connected to another vehicle, inspect the DLC3 of the original vehicle.
</p>
</div>
</div>
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>If communication is still not possible when the tester is connected to another vehicle, the problem may be in the tester itself. Consult the Service Department listed in the tester's instruction manual.
</p>
</div>
</div>
</div>
<br>
</dd>
</dl>
<br>
</div>
</div>
</div>
<br>
</div>
<br>
<dl class="topic">
<dt class="topic">FOR USING INTELLIGENT TESTER</dt>
<dd class="topic"><div class="list1">
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>Before using the intelligent tester, read the tester operator's manual thoroughly.
</p>
</div>
</div>
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>If the tester cannot communicate with the ECU controlled systems when connected to the DLC3 with the ignition switch in the on position and the tester turned on, there is a problem on the vehicle side or tester side.
</p>
</div>
</div>
<div class="list2">
<div class="list2Item">
<div class="list2Head">(a)</div>
<div class="list2Body"><p>If communication is possible when the tester is connected to another vehicle, inspect the diagnosis data link line (bus (+) line), CANH and CANL lines, and the power circuits for the vehicle ECUs.
</p>
</div>
</div>
<div class="list2Item">
<div class="list2Head">(b)</div>
<div class="list2Body"><p>If communication is still not possible when the tester is connected to another vehicle, the problem is probably in the tester itself. Perform the Self Test procedure outlined in the tester operator's manual.
</p>
</div>
</div>
</div>
<br>
</div>
<br>
</dd>
</dl>
<br>
</div>
</div>
</div>
</div>
<div id="footer"></div>
</div>
</div>
</body>
</html>
